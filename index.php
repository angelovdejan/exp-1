<?php

include 'src/Entity/User.php';
include 'src/Entity/Comment.php';
include 'src/Repository/InMemoryUsersRepository.php';
include 'src/Repository/InMemoryCommentsRepository.php';

////////////// setup

$users = new InMemoryUsersRepository();
$comments = new InMemoryCommentsRepository();

$first = new User();
$first->setName('User 1');
$users->store($first);

$comment = new Comment();
$comment->setContent('komentar 1');
$comments->store($comment);
$first->addComment($comment);

$comment = new Comment();
$comment->setContent('komentar 2');
$comments->store($comment);
$first->addComment($comment);

$second = new User();
$second->setName('User 2');
$comments->store($comment);
$users->store($second);

$comment = new Comment();
$comment->setContent('komentar 3');
$comments->store($comment);
$second->addComment($comment);

$comment = new Comment();
$comment->setContent('komentar 4');
$comments->store($comment);
$second->addComment($comment);


////////////// display


$all = $users->all();

foreach ($all as $user) {

    foreach ($user->getComments() as $comment) {
        echo sprintf(
            '%s (by %s)<br />',
            $comment->getContent(),
            $user->getName()
        );
    }

}