<?php

class InMemoryUsersRepository
{
    private $users;

    public function store(User $user)
    {
        $this->users[] = $user;
    }

    /**
     * @return User[]
     */
    public function all() : array
    {
        return $this->users;
    }
}
